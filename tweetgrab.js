Tweets = new Mongo.Collection('tweets');

var accounts = ["BarackObama","DalaiLama", "taylorswift13", "Cristiano", "JLo", "Oprah", "xxxxxxxx!!!!","ArianaGrande", "KAKA", "OfficialAdele", "jimmyfallon", "NiallOfficial","BillGates", "Eminem", "coldplay", "ActuallyNPH", "LeoDiCaprio", "ricky_martin", "10Ronaldinho", "vine"];

if(Meteor.isClient){
  Template.buttons.events({
    'click #get': function(){
      Meteor.call('callMultiTweets');
    },
    'click #clear': function(){
      Meteor.call('clear');
    }
  });

  Template.body.helpers({
    tweets: function(){
      return Tweets.find({},{sort:{inserted_at:-1}});
    }, 
    count: function(){
      return Tweets.find().count();
    }
  });
}


if(Meteor.isServer){
  Meteor.methods({
    clear: function(){
      Tweets.remove({});
    },
    callTweets: function(user){
      var startTwit = Meteor.wrapAsync(getTweets);
      try{
        var data = startTwit(user);
        addTweets(user, data);
      }
      catch(error){
        console.log("Error Getting Tweet for user" + user, error);
      }
    },
    callMultiTweets: function(){
      for(var x=0;x<accounts.length;x++){
        Meteor.call('callTweets', accounts[x]);
      }
    }
  });
}



var getTweets = function(account, callback){
      
    //Tweet-Grab Code Goes Here.
      Twit = new TwitMaker({
          consumer_key: '',
          consumer_secret:'',
          access_token:'',
          access_token_secret:''
      });

      Twit.get('statuses/user_timeline', 
        { screen_name: account, 
          count: 1, 
          include_rts:0
        },
        function (err, response){
          callback(err,response);
        });
};


var addTweets = function(account, response){
  Tweets.insert({
    account: account,
    message: response[0].text,
    time: response[0].created_at,
    inserted_at: Date.now()
    });
};